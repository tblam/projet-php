<?php 

require_once "lib/header_get.php";
require_once "lib/database.php";
require_once "lib/objects.php";

$database = new DB();
$db = $database->getConnection();

$filters = new Filters($db);

$competences = $filters->get_competences();
$filiere = $filters->get_filiere();
$niveau = $filters->get_niveau();

$all_filters['all-filters'] = array();
$records['competences'] = array();
$records['filieres'] = array();
$records['niveaux'] = array();

if ($niveau->rowCount()>0) {
    while ($row = $niveau->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $records_items = array(
                "id_niv" => $ID_Niv,
                "niveau" => $Niveau
        );
        array_push($records['niveaux'], $records_items);
    }
}

if ($filiere->rowCount()>0) {
    while ($row = $filiere->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $records_items = array(
                "nom_filiere" =>$Nom_Filiere,
                "logo" => $Logo,
                "label" => $Label,
                "Localisation" => array(
                    "x_axis" => $X_axis,
                    "y_axis" => $Y_axis,
                    "nom_loc" => $Nom_Loc
                )
        );
        array_push($records['filieres'], $records_items);
    }
}
if($competences->rowCount()>0){
    while ($row = $competences->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $records_items = array(
            "id" => $ID_comp,
            "titre" => $Titre,
            "desc" => $Desc_Comp,
            "categorie" => array(
                "id_cat"=> $ID_Cat,
                "titre_cat" => $Titre_Cat,
            ),
        );
        array_push($records['competences'], $records_items);
    }
    array_push($all_filters['all-filters'], $records['competences']);
    // set response code - 200 OK
    http_response_code(200);
    echo json_encode($records);
}
else{
    http_response_code(404);
    echo json_encode(
        array("error" => "Pas de compétence trouvée")
    );
}
?>