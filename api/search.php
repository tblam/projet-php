<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once "lib/header_get.php";
include_once "lib/database.php";
include_once "lib/objects.php";
  
$database = new DB();
$db = $database->getConnection();
$etudiants = new Etudiants($db);




$keywords=isset($_GET['s']) ? $_GET['s'] : "";


$stmt = $etudiants->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  

    $records['records'] = array();
  
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
  
        $records_items = array(
            "id" => $Num_Etud,
            "nom" => $Nom,
            "prenom" => $Prenom,
            "photo" => $Photo,
            "niveau" => $Niveau,
            "comp" => explode(",", $Competences),
            "spe" => explode(",", $Spe)
        );
  
        array_push($records['records'], $records_items);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show products data
    echo json_encode($records);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no products found
    echo json_encode(
        array("error" => "Pas d'étudiant trouvé.")
    );
}
?>