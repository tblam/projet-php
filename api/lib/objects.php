<?php
class Etudiants{

    // database connection and table name
    private $conn;
    private $table_name = "etudiants";

    // object properties
    //etudiants
    public $Num_Etud;
    public $Nom;
    public $Prenom;
    public $Poids;
    public $Email;
    public $Facebook;
    public $Instagram;
    public $Twitter;
    public $Photo;
    public $Taille;
    //Niveau de l'etudiant
    public $ID_Niv;
    public $Discord;
    public $Description;
    public $Niveau;
    //Compétences de l'etudiant
    public $ID_Comp;
    public $ID_Etud;
    public $Niveau_Comp;
    public $Titre;
    public $Desc_Comp;

    Public $Competences;
    Public $Spe;

    public $ID_Cat;
    public $Categorie;
    public $Titre_Cat;
    //filiere et localisation de l'etudiant
    public $ID_Filiere;
    public $Nom_Filiere;
    public $Label;
    public $Logo;
    public $X_axis;
    public $Y_axis;
    public $Nom_Loc;


  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function read_all(){
        // select all query
        $query = "SELECT *, GROUP_CONCAT(competences.ID_Cat) AS Spe, GROUP_CONCAT(DISTINCT competences.ID_comp) AS Competences FROM junct_comp_etud INNER JOIN etudiants ON junct_comp_etud.ID_Etud = etudiants.Num_Etud INNER JOIN competences ON competences.ID_comp = junct_comp_etud.ID_Comp INNER JOIN niveau ON etudiants.ID_Niv = niveau.ID_Niv GROUP BY etudiants.Num_Etud ORDER BY etudiants.Nom";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }


    function read_one(){
        // select Num_etud query
        $query = "SELECT * FROM `etudiants` RIGHT JOIN niveau ON etudiants.ID_Niv = niveau.ID_Niv RIGHT JOIN filiere ON etudiants.ID_Filiere = filiere.ID_Filiere RIGHT JOIN location ON filiere.ID_Loc = location.ID_Loc WHERE Num_Etud = "."$this->Num_Etud";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->Num_Etud);
        // execute query
        $stmt->execute();
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->Num_Etud = $row['Num_Etud'];
        $this->Nom = $row['Nom'];
        $this->Prenom = $row['Prenom'];
        $this->Niveau = $row['Niveau'];
        $this->Poids = $row['Poids'];
        $this->Email = $row['Email'];
        $this->Facebook = $row['Facebook'];
        $this->Instagram = $row['Instagram'];
        $this->Twitter = $row['Twitter'];
        $this->Photo = $row['Photo'];
        $this->Taille = $row['Taille'];
        $this->Discord = $row['Discord'];
        $this->Description = $row['Description'];
        $this->Nom_Filiere = $row['Nom_Filiere'];
        $this->Label = $row['Label'];
        $this->Logo = $row['Logo'];
        $this->X_axis = $row['X_axis'];
        $this->Y_axis = $row['Y_axis'];
        $this->Nom_Loc = $row['Nom_Loc'];
    }
    function get_competences(){
        $query = "SELECT *, cat_competence.Titre_Cat FROM `junct_comp_etud` RIGHT JOIN competences ON junct_comp_etud.ID_Comp = competences.ID_comp RIGHT JOIN cat_competence ON competences.ID_Cat = cat_competence.ID_Cat WHERE junct_comp_etud.ID_Etud = "."$this->Num_Etud";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->Num_Etud);
        // execute query
        $stmt->execute();
        return $stmt;
    }
    
    function add_etud(){

        // query to insert record
        $query = "INSERT INTO ".$this->table_name."
        VALUES ("."'".$this->Num_Etud."'".",
        "."'".$this->Nom."'".",
        "."'".$this->Prenom."'".",
        "."'".$this->ID_Niv."'".",
        "."'".$this->Poids."'".",
        "."'".$this->Email."'".",
        "."'".$this->Facebook."'".",
        "."'".$this->Instagram."'".",
        "."'".$this->Twitter."'".",
        "."'".$this->ID_Filiere."'".",
        "."'".$this->Photo."'".",
        "."'".$this->Taille."'".",
        "."'".$this->Discord."'".",
        "."'".$this->Description."'"."
        )";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize (évite les injections SQL)
        $this->Num_Etud=htmlspecialchars(strip_tags($this->Num_Etud));
        $this->Nom=htmlspecialchars(strip_tags($this->Nom));
        $this->Prenom=htmlspecialchars(strip_tags($this->Prenom));
        $this->ID_Niv=htmlspecialchars(strip_tags($this->ID_Niv));
        $this->Poids=htmlspecialchars(strip_tags($this->Poids));
        $this->Email=htmlspecialchars(strip_tags($this->Email));
        $this->Facebook=htmlspecialchars(strip_tags($this->Facebook));
        $this->Instagram=htmlspecialchars(strip_tags($this->Instagram));
        $this->Twitter=htmlspecialchars(strip_tags($this->Twitter));
        $this->ID_Filiere=htmlspecialchars(strip_tags($this->ID_Filiere));
        $this->Photo=htmlspecialchars(strip_tags($this->Photo));
        $this->Taille=htmlspecialchars(strip_tags($this->Taille));
        $this->Discord=htmlspecialchars(strip_tags($this->Discord));
        $this->Description=htmlspecialchars(strip_tags($this->Description));


        // bind values
        $stmt->bindParam(":Num_Etud", $this->Num_Etud, PDO::PARAM_STR);
        $stmt->bindParam(":Nom", $this->Nom, PDO::PARAM_STR);
        $stmt->bindParam(":Prenom", $this->Prenom, PDO::PARAM_STR);
        $stmt->bindParam(":ID_Niv", $this->ID_Niv, PDO::PARAM_INT);
        $stmt->bindParam(":Poids", $this->Poids, PDO::PARAM_INT);
        $stmt->bindParam(":Email", $this->Email, PDO::PARAM_STR);
        $stmt->bindParam(":Facebook", $this->Facebook, PDO::PARAM_STR);
        $stmt->bindParam(":Instagram", $this->Instagram, PDO::PARAM_STR);
        $stmt->bindParam(":Twitter", $this->Twitter, PDO::PARAM_STR);
        $stmt->bindParam(":ID_Filiere", $this->ID_Filiere, PDO::PARAM_INT);
        $stmt->bindParam(":Photo", $this->Photo, PDO::PARAM_STR);
        $stmt->bindParam(":Taille", $this->Taille, PDO::PARAM_STR);
        $stmt->bindParam(":Discord", $this->Discord, PDO::PARAM_STR);
        $stmt->bindParam(":Description", $this->Description, PDO::PARAM_STR);


        if($stmt->execute()){
            return true;
        }
        else{
            return false;
        }
    }

    public function add_competence($data){
        $query = "INSERT INTO `junct_comp_etud` (ID_Etud, ID_Comp, Niveau_Comp) VALUE ";
        $elem = array();
        for ($i=0; $i < sizeof($data); $i++) {
            $text = '('.$data[$i]->Num_Etud.','.$data[$i]->ID_Comp.','.$data[$i]->Niveau_Comp.')';
            array_push($elem, $text);
        }
        $insert_value = implode(',', $elem);

        $query .= $insert_value;   
        $stmt = $this->conn->prepare($query);

        if($stmt->execute()){
            return true;
        }
        else{
            return false;
        }

    }

    function search($keywords){
        // select all query
        $search_sql = "SELECT *, GROUP_CONCAT(competences.ID_Cat) AS Spe, GROUP_CONCAT(DISTINCT competences.ID_comp) AS Competences FROM junct_comp_etud INNER JOIN etudiants ON junct_comp_etud.ID_Etud =etudiants.Num_Etud INNER JOIN competences ON competences.ID_comp = junct_comp_etud.ID_Comp INNER JOIN niveau ON etudiants.ID_Niv = niveau.ID_Niv WHERE";
        $keyword = explode(" ", $keywords); 
        $keyCount = 0;
        foreach ($keyword as $keys) { 
            if ($keyCount > 0){
                $search_sql .= " AND ";
            }
            $search_sql .= " junct_comp_etud.ID_Etud LIKE '$keys' OR junct_comp_etud.ID_Etud IN (SELECT Num_Etud FROM etudiants WHERE Prenom = '$keys') OR junct_comp_etud.ID_Etud IN (SELECT Num_Etud FROM etudiants WHERE Nom = '$keys') ";
            ++$keyCount;
            ;
        }
        
        $query = $search_sql."GROUP BY etudiants.Num_Etud ORDER BY etudiants.Nom";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
    
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);
    
        // execute query
        $stmt->execute();
        return $stmt;
    }
}

class Signalement{

    // database connection and table name
    private $conn;
    private $table_name = "`signalement`";

    // object properties
    // public $ID_Sign;
    public $Email ;
    public $Type;
    public $Titre;
    public $Message;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read signalements
    function read_all(){
        // select all query
        $query = "SELECT * FROM ".$this->table_name;
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();

        return $stmt;
    }

    function add_sign(){
        // query to insert record
        $query = "INSERT INTO ".$this->table_name." (`Email`, `Type`, `Titre`, `Message`)
        VALUES ("."'".$this->Email."'".",
        "."'".$this->Type."'".",
        "."'".$this->Titre."'".",
        "."'".$this->Message."'".")";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize (évite les injections SQL)
        $this->Email=htmlspecialchars(strip_tags($this->Email));
        $this->Type=htmlspecialchars(strip_tags($this->Type));
        $this->Titre=htmlspecialchars(strip_tags($this->Titre));
        $this->Message=htmlspecialchars(strip_tags($this->Message));

        // bind values
        $stmt->bindParam(":Email", $this->Email, PDO::PARAM_STR);
        $stmt->bindParam(":Type", $this->Type, PDO::PARAM_STR);
        $stmt->bindParam(":Titre", $this->Titre, PDO::PARAM_INT);
        $stmt->bindParam(":Message", $this->Message, PDO::PARAM_INT);


        var_dump($stmt);

        if($stmt->execute()){
            return true;
        }
        else{
            return false;
        }
    }
}
class Filters {
        private $conn;

        //Niveau de l'etudiant
        public $ID_Niv;
        public $Niveau;
        //Compétences de l'etudiant
        public $ID_Comp;
        public $Titre;
        public $Desc_Comp;

        public $ID_Cat;
        public $Titre_Cat;
        //filiere et localisation de l'etudiant
        public $ID_Filiere;
        public $Nom_Filiere;
        public $Label;
        public $Logo;
        public $X_axis;
        public $Y_axis;
        public $Nom_Loc;

        public function __construct($db){
            $this->conn = $db;
        }

        function get_competences(){
        $query = "SELECT DISTINCT *, cat_competence.Titre_Cat FROM `competences` RIGHT JOIN cat_competence ON cat_competence.ID_Cat = competences.ID_Cat GROUP BY competences.ID_comp";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
        }
        function get_niveau(){
            $query = "SELECT * FROM `niveau`";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }
        function get_filiere(){
            $query = "SELECT * FROM `filiere` RIGHT JOIN location ON filiere.ID_Loc = location.ID_Loc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }
}
?>