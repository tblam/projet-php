<?php
require_once "lib/header_get.php";
require_once "lib/database.php";
require_once "lib/objects.php";

$database = new DB();
$db = $database->getConnection();

// initialIze object
$etudiants = new Etudiants($db);
$filters = new Filters($db);

$getCompetences = $filters->get_competences();
$rowComp = $getCompetences->rowCount();
// query etudiants
$stmt = $etudiants->read_all();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
    $records['records'] = array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $records_items = array(
            "id" => $Num_Etud,
            "nom" => $Nom,
            "prenom" => $Prenom,
            "photo" => $Photo,
            "niveau" => $Niveau,
            "comp" => explode(",", $Competences),
            "spe" => explode(",", $Spe)
        );

        array_push($records['records'], $records_items);
    }

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($records);
}

else{

    http_response_code(404);

    echo json_encode(
        array("error" => "Pas d'étudiant trouvé")
    );
}

?>
