<?php
require_once "lib/header_post.php";
require_once "lib/database.php";
require_once "lib/objects.php";

$database = new DB();
$db = $database->getConnection();
$etudiants = new Etudiants($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data)
){

    $etudiants->Num_Etud = $data->Num_Etud;
    $etudiants->Nom = $data->Nom;
    $etudiants->Prenom = $data->Prenom;
    $etudiants->ID_Niv = $data->ID_Niv;
    $etudiants->Poids = $data->Poids;
    $etudiants->Email = $data->Email;
    $etudiants->Facebook = $data->Facebook;
    $etudiants->Instagram = $data->Instagram;
    $etudiants->Twitter = $data->Twitter;
    $etudiants->ID_Filiere = $data->ID_Filiere;
    $etudiants->Photo = $data->Photo;
    $etudiants->Taille = $data->Taille;
    $etudiants->Discord = $data->Discord;
    $etudiants->Description = $data->Description;

    $etudiants->add_competence($data->Competences);

    if($etudiants->add_etud() && $etudiants->add_competence($data->Competences)){
        http_response_code(201);
        echo json_encode(array("error" => "Etudiant was created."));
    }
    else{
        http_response_code(503);
        echo json_encode(array("error" => "Unable to create etudiant."));
    }
}
else{

    http_response_code(400);
    echo json_encode(array("error" => "Unable to create etudiant. Data are incomplete."));
}

?>
