<?php
    // include database and object files
    include_once "lib/header_get.php";
    include_once "lib/database.php";
    include_once "lib/objects.php";

    // get database connection
    $database = new DB();
    $db = $database->getConnection();

    // initialIze object
    $etudiants = new Etudiants($db);

    // query Etudiants
    $etudiants->Num_Etud = isset($_GET['Num_Etud']) ? $_GET['Num_Etud'] : die();

    $etudiants->read_one();

    if($etudiants->Nom!=null){
        $stmt = $etudiants->get_competences();
        $num = $stmt->rowCount();
        $records = [];
        $records['competences'] = array();
        if($num>0){
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $skills_items = array(
                    "titre" => $Titre,
                    "niveau" => $Niveau_Comp,
                    "desc" => $Desc_Comp,
                    "categorie" => array(
                        "ID_Cat" => $ID_Cat,
                        "Titre_Cat" => $Titre_Cat,
                    )
                );
                array_push($records['competences'], $skills_items);
            }
        }
        $etud_seul_arr = array(
            "num_Etud" => $etudiants->Num_Etud,
            "nom" => $etudiants->Nom,
            "prenom" => $etudiants->Prenom,
            "poids" => $etudiants->Poids,
            "niveau" => $etudiants->Niveau,
            "email" => $etudiants->Email,
            "facebook" => $etudiants->Facebook,
            "instagram" => $etudiants->Instagram,
            "twitter" => $etudiants->Twitter,
            "photo" => $etudiants->Photo,
            "taille" => $etudiants->Taille,
            "discord" =>$etudiants->Discord,
            "description" =>$etudiants->Description,
            "filiere" => array(
                "nom_filiere" =>$etudiants->Nom_Filiere,
                "logo" => $etudiants->Logo,
                "label" => $etudiants->Label,
                "Localisation" => array(
                    "x_axis" => $etudiants->X_axis,
                    "y_axis" => $etudiants->Y_axis,
                    "nom_loc" => $etudiants->Nom_Loc
                )
                ),
                "competences" => $records['competences']
        );
    // set response code - 200 OK
    // set response code - 200 OK
    http_response_code(200);

    // make it json format
    echo json_encode($etud_seul_arr);

    }

    else{
        http_response_code(404);
        echo json_encode(
            array("error" => "Pas d'étudiant trouvé")
        );
    }

?>