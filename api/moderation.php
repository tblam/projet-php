<?php
require_once "lib/header_get.php";
require_once "lib/database.php";
require_once "lib/objects.php";

$database = new DB();
$db = $database->getConnection();

// initialIze object
$stmt2 = $signalement->read_all();
$num2 = $stmt2->rowCount();
if (num2>0){

  $sign_arr=array();
  $sign_arr["reports"]=array();

  while ($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
      extract($row);

      $sign_item=array(
          "id_sign" => $ID_Sign,
          "email" => $Email,
          "type" => $Type,
          "titre" => $Titre,
          "message" => $Message,
      );

      array_push($sign_arr["reports"], $sign_item);
  }

  // set response code - 200 OK
  http_response_code(200);

  // show products data in json format
  echo json_encode($sign_arr);
}
else{

  http_response_code(404);

  echo json_encode(
      array("error" => "Pas de signalement trouvé")
  );
}

?>
