-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 31 mai 2020 à 11:49
-- Version du serveur :  10.1.40-MariaDB
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `le_trombi`
--

-- --------------------------------------------------------

--
-- Structure de la table `cat_competence`
--

CREATE TABLE `cat_competence` (
  `ID_Cat` int(12) NOT NULL,
  `Titre_Cat` varchar(65) DEFAULT NULL,
  `Description` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cat_competence`
--

INSERT INTO `cat_competence` (`ID_Cat`, `Titre_Cat`, `Description`) VALUES
(1, 'Développement Web', 'Développement de services pour le web'),
(2, 'Programmation', 'Maîtrise de langages de programmation.'),
(3, 'Arts & Graphisme', 'Création graphique.'),
(4, 'Audiovisuel', 'Création de contenu audiovisuel'),
(5, 'Sciences de l\'ingénieur', 'Sciences liées à l\'ingénierie.'),
(6, '3D', 'Modélisation et animation 3D.');

-- --------------------------------------------------------

--
-- Structure de la table `competences`
--

CREATE TABLE `competences` (
  `ID_comp` int(12) NOT NULL,
  `Titre` varchar(65) NOT NULL,
  `Desc_Comp` char(255) NOT NULL,
  `ID_Cat` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `competences`
--

INSERT INTO `competences` (`ID_comp`, `Titre`, `Desc_Comp`, `ID_Cat`) VALUES
(1, 'MYSQL', 'Service de bases de données', 1),
(2, 'Adobe Photoshop', 'Logiciel de montage photo', 3),
(3, 'Adobe Premiere', 'Logiciel de montage vidéo', 4),
(4, 'Adobe Illustrator', 'Logiciel de design vectoriel', 3),
(5, 'Adobe After Effect', 'Logiciel de composition vidéo', 4),
(6, 'Développements limités', 'C\'est la mer noire', 5),
(7, 'Division de matrices', 'Diviser des matrices de nombres', 5),
(8, 'C++', 'Langage de programmation orienté objet', 2),
(9, 'Python', 'Langage de programmation stylé', 2),
(10, 'HTML', 'Langage de balisage web', 1),
(11, 'Vue.js', 'Framework javascript pour frontend', 1),
(12, 'Blender', 'Logiciel Libre de modélisation et animation 3D', 6),
(13, 'Cinema 4D', 'Logiciel de modélisation et animation 3D', 6),
(14, '3DS Max', 'Logiciel de modélisation et animation 3D', 6),
(15, 'FL Studio', 'Logiciel de MAO (Musique Assistée par Ordinateur)', 3),
(16, 'Algèbre linéaire', 'Mathématiques algébrique linéaire.', 5),
(17, 'Traitement du signal', 'Manipulation mathématiques de signaux.', 5);

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

CREATE TABLE `etudiants` (
  `Num_Etud` varchar(12) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `ID_Niv` int(11) NOT NULL,
  `Poids` int(11) NOT NULL,
  `Email` varchar(65) NOT NULL,
  `Facebook` varchar(50) NOT NULL,
  `Instagram` varchar(50) NOT NULL,
  `Twitter` varchar(50) NOT NULL,
  `ID_Filiere` int(12) NOT NULL,
  `Photo` varchar(1024) NOT NULL,
  `Taille` int(11) NOT NULL,
  `Discord` varchar(50) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`Num_Etud`, `Nom`, `Prenom`, `ID_Niv`, `Poids`, `Email`, `Facebook`, `Instagram`, `Twitter`, `ID_Filiere`, `Photo`, `Taille`, `Discord`, `Description`) VALUES
('207843', 'Rougnon-Glasson', 'Alaric', 4, 1730, 'alaric.rougnon.glasson@gmail.com', 'https://www.facebook.com/alaricrg', '@alaricrg', '@akimoul', 7, 'https://i.pinimg.com/280x280_RS/d2/0f/48/d20f48987058d4ff04d1869ea1c8d982.jpg', 846, 'Alaric#5810', 'Étudiant de type IMAC 2022. Respo Jeudimac et vice-président de 8sang3Z de la veine.');

-- --------------------------------------------------------

--
-- Structure de la table `filiere`
--

CREATE TABLE `filiere` (
  `ID_Filiere` int(12) NOT NULL,
  `Nom_Filiere` varchar(65) NOT NULL,
  `Label` varchar(12) NOT NULL,
  `Logo` char(255) NOT NULL,
  `ID_Loc` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `filiere`
--

INSERT INTO `filiere` (`ID_Filiere`, `Nom_Filiere`, `Label`, `Logo`, `ID_Loc`) VALUES
(7, 'Image Multimédia Audiovisuel Communication', 'IMAC', 'https://s2.qwant.com/thumbr/0x0/6/e/d3e0ff4d6d4c0fd6700a23938e48e3a66b0cdab125f8b8eb7e0ad94de575d1/logoIMAC.png?u=http%3A%2F%2Fwww.ingenieur-imac.fr%2Fimages%2FlogoIMAC.png&q=0&b=1&p=0&a=1', 1),
(8, 'Génie civil', 'GC', 'https://s1.qwant.com/thumbr/0x380/3/a/dfea2b6d6c4611368ce91db476a79a04c2f8c542dc24bf5dbd632dfe6ef1db/maxresdefault.jpg?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fvbm3EXpUUcM%2Fmaxresdefault.jpg&q=0&b=1&p=0&a=1', 2),
(9, 'Informatique voilà', 'INFO', 'https://fa.wikipedia.org/wiki/%D8%B4%D9%86%DA%AF_%D9%87%D9%86%D8%AF%DB%8C', 3);

-- --------------------------------------------------------

--
-- Structure de la table `junct_comp_etud`
--

CREATE TABLE `junct_comp_etud` (
  `ID_Type_Comp` int(12) NOT NULL,
  `ID_Etud` varchar(12) NOT NULL,
  `ID_Comp` int(12) NOT NULL,
  `Niveau_Comp` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `junct_comp_etud`
--

INSERT INTO `junct_comp_etud` (`ID_Type_Comp`, `ID_Etud`, `ID_Comp`, `Niveau_Comp`) VALUES
(36, '207843', 1, 3),
(37, '207843', 10, 5),
(38, '207843', 11, 4),
(39, '207843', 2, 4),
(40, '207843', 4, 3),
(41, '207843', 15, 2),
(42, '207843', 3, 3),
(43, '207843', 5, 3),
(44, '207843', 8, 3),
(45, '207843', 12, 2);

-- --------------------------------------------------------

--
-- Structure de la table `location`
--

CREATE TABLE `location` (
  `ID_Loc` int(12) NOT NULL,
  `X_axis` float NOT NULL,
  `Y_axis` float NOT NULL,
  `Nom_Loc` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `location`
--

INSERT INTO `location` (`ID_Loc`, `X_axis`, `Y_axis`, `Nom_Loc`) VALUES
(1, 7, 7, 'Copernic'),
(2, 10, 10, 'Lavoisier'),
(3, 12, 12, 'Bois de l\'étang');

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `ID_Niv` int(11) NOT NULL,
  `Niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`ID_Niv`, `Niveau`) VALUES
(1, 0),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 1),
(8, 2),
(9, 3),
(10, 4),
(11, 5);

-- --------------------------------------------------------

--
-- Structure de la table `signalement`
--

CREATE TABLE `signalement` (
  `ID_Sign` int(12) NOT NULL,
  `Email` char(65) NOT NULL,
  `Type` int(12) NOT NULL,
  `Titre` char(65) NOT NULL,
  `Message` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_sign`
--

CREATE TABLE `type_sign` (
  `ID_Type_Sign` int(11) NOT NULL,
  `Titre` char(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_sign`
--

INSERT INTO `type_sign` (`ID_Type_Sign`, `Titre`) VALUES
(3, 'Bug'),
(1, 'Contenu innaproprié'),
(4, 'Proposition'),
(2, 'Usurpation d\'identité');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cat_competence`
--
ALTER TABLE `cat_competence`
  ADD PRIMARY KEY (`ID_Cat`);

--
-- Index pour la table `competences`
--
ALTER TABLE `competences`
  ADD PRIMARY KEY (`ID_comp`),
  ADD KEY `ID_Cat` (`ID_Cat`);

--
-- Index pour la table `etudiants`
--
ALTER TABLE `etudiants`
  ADD PRIMARY KEY (`Num_Etud`),
  ADD KEY `Filiere` (`ID_Filiere`),
  ADD KEY `Niveau` (`ID_Niv`),
  ADD KEY `Niveau_2` (`ID_Niv`);

--
-- Index pour la table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`ID_Filiere`),
  ADD KEY `ID_Loc` (`ID_Loc`);

--
-- Index pour la table `junct_comp_etud`
--
ALTER TABLE `junct_comp_etud`
  ADD PRIMARY KEY (`ID_Type_Comp`),
  ADD KEY `ID_Etud` (`ID_Etud`),
  ADD KEY `ID_Comp` (`ID_Comp`);

--
-- Index pour la table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`ID_Loc`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`ID_Niv`);

--
-- Index pour la table `signalement`
--
ALTER TABLE `signalement`
  ADD PRIMARY KEY (`ID_Sign`),
  ADD KEY `Type` (`Type`);

--
-- Index pour la table `type_sign`
--
ALTER TABLE `type_sign`
  ADD PRIMARY KEY (`ID_Type_Sign`),
  ADD KEY `Titre` (`Titre`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cat_competence`
--
ALTER TABLE `cat_competence`
  MODIFY `ID_Cat` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `competences`
--
ALTER TABLE `competences`
  MODIFY `ID_comp` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `ID_Filiere` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `junct_comp_etud`
--
ALTER TABLE `junct_comp_etud`
  MODIFY `ID_Type_Comp` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT pour la table `location`
--
ALTER TABLE `location`
  MODIFY `ID_Loc` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `ID_Niv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `signalement`
--
ALTER TABLE `signalement`
  MODIFY `ID_Sign` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `type_sign`
--
ALTER TABLE `type_sign`
  MODIFY `ID_Type_Sign` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `competences`
--
ALTER TABLE `competences`
  ADD CONSTRAINT `competences_ibfk_1` FOREIGN KEY (`ID_Cat`) REFERENCES `cat_competence` (`ID_Cat`);

--
-- Contraintes pour la table `etudiants`
--
ALTER TABLE `etudiants`
  ADD CONSTRAINT `etudiants_ibfk_1` FOREIGN KEY (`ID_Niv`) REFERENCES `niveau` (`ID_Niv`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `etudiants_ibfk_2` FOREIGN KEY (`ID_Filiere`) REFERENCES `filiere` (`ID_Filiere`);

--
-- Contraintes pour la table `filiere`
--
ALTER TABLE `filiere`
  ADD CONSTRAINT `filiere_ibfk_1` FOREIGN KEY (`ID_Loc`) REFERENCES `location` (`ID_Loc`);

--
-- Contraintes pour la table `junct_comp_etud`
--
ALTER TABLE `junct_comp_etud`
  ADD CONSTRAINT `junct_comp_etud_ibfk_1` FOREIGN KEY (`ID_Etud`) REFERENCES `etudiants` (`Num_Etud`),
  ADD CONSTRAINT `junct_comp_etud_ibfk_2` FOREIGN KEY (`ID_Comp`) REFERENCES `competences` (`ID_comp`);

--
-- Contraintes pour la table `signalement`
--
ALTER TABLE `signalement`
  ADD CONSTRAINT `Type_sign` FOREIGN KEY (`Type`) REFERENCES `type_sign` (`ID_Type_Sign`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
