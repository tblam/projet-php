<?php 
require_once "lib/header_post.php";
require_once "lib/database.php";
require_once "lib/objects.php";

$database = new DB();
$db = $database->getConnection();
$signalement = new Signalement($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
var_dump($data);
if(
    !empty($data)
){
    $signalement->Email = $data->Email;
    $signalement->Type = $data->Type;
    $signalement->Titre = $data->Titre;
    $signalement->Message = $data->Message;

    var_dump($signalement);
  

    if($signalement->add_sign()){
        http_response_code(201);
        echo json_encode(array("error" => "Report was created."));
    }
    else{
        http_response_code(503);
        echo json_encode(array("error" => "Unable to create report."));
    }
}
else{

    http_response_code(400);
    echo json_encode(array("error" => "Unable to create report. Data are incomplete."));
}

?>