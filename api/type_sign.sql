-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 31 mai 2020 à 10:51
-- Version du serveur :  10.1.40-MariaDB
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `le_trombi`
--

-- --------------------------------------------------------

--
-- Structure de la table `type_sign`
--

CREATE TABLE `type_sign` (
  `ID_Type_Sign` int(11) NOT NULL,
  `Titre` char(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_sign`
--

INSERT INTO `type_sign` (`ID_Type_Sign`, `Titre`) VALUES
(3, 'Bug'),
(1, 'Contenu innaproprié'),
(4, 'Proposition'),
(2, 'Usurpation d\'identité');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `type_sign`
--
ALTER TABLE `type_sign`
  ADD PRIMARY KEY (`ID_Type_Sign`),
  ADD KEY `Titre` (`Titre`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `type_sign`
--
ALTER TABLE `type_sign`
  MODIFY `ID_Type_Sign` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
