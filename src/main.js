import Vue from 'vue'
import App from './App.vue'
// import Axios from 'axios'
// Importation de vuesax
import Vuesax3 from 'vuesax3'
import 'vuesax3/dist/vuesax.css'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
// Importation material icons
import 'material-icons/iconfont/material-icons.css'
import VueRouter from 'vue-router'
// Importation de Vuex
import Vuex from 'vuex'
import Repertoire from './components/Repertoire'
import StudentFormular from './components/StudentFormular'
import ReportFormular from './components/ReportFormular'
import Profil from './components/Profil'
Vue.use(Vuex)

// Gestion des données récupérées de l'API
const moduleData = {
  state: {
    displayedStudents: [],
    research: '',
    switchSort: 2,
    maxSpe: 0
  },
  mutations: {
    removeStudents (numList, state) {
      for (const student of state.displayedStudents) {
        if (numList.includes(student.id)) {
          state.displayedStudents.splice(state.displayedStudents.find(ele => ele.num_etud == student.id), 1)
          console.log('Removed ' + student.id)
        }
      }
    },
    setResearch (state, researchInput) {
      state.research = researchInput
    },
    resetResearch (state) {
      state.research = ''
    },
    changeSort (state, newSort) {
      state.switchSort = newSort
    },
    addStudent (state, newStudent) {
      // console.log("Pushed ", newStudent);
      state.displayedStudents.push(newStudent)
    },
    resetDisplayedStudents (state) {
      state.displayedStudents = []
    },
    returnMostOccurence (state, array) {
      console.log('maxSpeAvant : ', moduleData.state.maxSpe)
      console.log('student : ', array)
      if (array.length == 0) { return null }
      const modeMap = {}
      let maxEl = array[0]; let maxCount = 1
      for (let i = 0; i < array.length; i++) {
        const el = array[i]
        if (modeMap[el] == null) { modeMap[el] = 1 } else { modeMap[el]++ }
        if (modeMap[el] > maxCount) {
          maxEl = el
          maxCount = modeMap[el]
        }
      }
      state.maxSpe = maxEl
      console.log('maxSpeAprès : ', moduleData.state.maxSpe)
    }
  },
  actions: {
    addStudents ({ commit, rootState }, studentsList) {
      for (const student of studentsList) {
        const newStudent = student
        commit('returnMostOccurence', newStudent.spe)
        switch (moduleData.state.maxSpe) {
          case '1':
            newStudent.color = rootState.colors.web
            newStudent.spe = 'web'
            break
          case '2':
            newStudent.color = rootState.colors.prog
            newStudent.spe = 'prog.'
            break
          case '3':
            newStudent.color = rootState.colors.art
            newStudent.spe = 'art'
            break
          case '4':
            newStudent.color = rootState.colors.audiovisuel
            newStudent.spe = 'audiovisuel'
            break
          case '5':
            newStudent.color = rootState.colors.scInge
            newStudent.spe = 'sc. inge'
            break
          case '6':
            newStudent.color = rootState.colors.D3
            newStudent.spe = '3D'
            break
          default :
            newStudent.color = '0'
            break
        }
        commit('addStudent', newStudent)
      }
    }
  }
}

// Palette de couleur / Module
const moduleColors = {
  state: {
    default: '#7e7e7e',
    second: '#c4c4c4',
    audiovisuel: '#ffca7b',
    art: '#ff8585',
    web: '#8ccfff',
    prog: '#7fde70',
    scInge: '#d2a6ff',
    D3: '#d09c86',
    bg1: '#ffffff',
    bg2: '#f9f9f9',
    sinBg1: '#c01f1f',
    singBg2: '#f9f9f9',
    mainTitle: '#525252',
    primary: '#8684DF'
  }
}

const moduleInterface = {
  state: {
    menuIsOpen: false,
    filters: {
      spe: {
        audiovisuel: false,
        web: false,
        scInge: false,
        art: false,
        programmation: false,
        D3: false
      },
      niv: {
        min: '',
        max: ''
      },
      filieres: []
    }
  },
  mutations: {
    toggleMenu (state) {
      state.menuIsOpen = !state.menuIsOpen
      // console.log("menuIsOpen :", state.menuIsOpen)
    },
    toggleFilterSpe (state, toggle) {
      switch (toggle) {
        case 'audiovisuel':
          state.filters.spe.audiovisuel = !state.filters.spe.audiovisuel
          break
        case 'web':
          state.filters.spe.web = !state.filters.spe.web
          break
        case 'scInge':
          state.filters.spe.scInge = !state.filters.spe.scInge
          break
        case 'art':
          state.filters.spe.art = !state.filters.spe.art
          break
        case 'programmation':
          state.filters.spe.programmation = !state.filters.spe.programmation
          break
        case 'D3':
          state.filters.spe.D3 = !state.filters.spe.D3
          break
      }
    },
    setFilterNiv (state, values) {
      // console.log('min/max : ', values.min, values.max)
      state.filters.niv.min = values.min
      state.filters.niv.max = values.max
      // console.log("Setting filter niv to :"+state.filters.niv.min+" "+state.filters.niv.max)
    },
    setFilterFiliere (state, list) {
      state.filters.filiere = list
      console.log('Setting filter filiere to :', state.filters.filiere)
    },
    setFilterPosition (state, list) {
      state.filters.position = list
      console.log('Setting filter position to :', state.filters.position)
    }
  },
  getters: {
    // Renvoie false si des filtres sont définis
    isFilters: state => {
      const filters = state.filters
      // console.log(filters)
      if (filters.spe.length == 0 && filters.niv.min == '' && filters.niv.max == '' && filters.filieres.length == 0 && filters.position.length == 0) {
        // console.log("isFilters FALSE")
        return false
      } else {
        // console.log("isFilters TRUE")
        return true
      }
    }
  }
}

const store = new Vuex.Store({
  modules: {
    colors: moduleColors,
    interface: moduleInterface,
    data: moduleData
  }
})

const vuesaxColors = {
  primary: '#8684DF',
  success: 'rgb(23, 201, 100)',
  danger: 'rgb(242, 19, 93)',
  warning: 'rgb(255, 130, 0)',
  dark: 'rgb(36, 33, 69)'
}

Vue.use(Vuesax, {
  theme: {
    colors: vuesaxColors
  }
})

Vue.use(Vuesax3, {
  theme: {
    colors: vuesaxColors
  }
})

// Set up Vue-Router
Vue.use(VueRouter)

const routes = [
  { path: '/repertoire', component: Repertoire },
  { path: '/addstudent', component: StudentFormular },
  { path: '/report', component: ReportFormular },
  { path: '*', redirect: '/repertoire' },
  // Custom path for students profile
  { path: '/user/:num_etud', component: Profil }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})
//

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store,
  router,
  created: function () {
    for (const color in this.$store.state.colors) {
      document.documentElement.style.setProperty('--' + color, this.$store.state.colors[color])
    }
  }
}).$mount('#app')
