# Beji

À l'IMAC, on n'attrape pas de pokémons. En revanche, on cherche souvent des camarades pour des projets ... C'est pourquoi nous avons décidé de créer le tout premier **Pokédex des étudiants** ! Vous pourrez y découvrir tous les étudiants, leurs capacités et des conseils pour les ~~attraper~~ contacter !

# projet-php

## Installation du projet
```
npm install
```

## Utilisation
### Lancer le serveur de développement de vue-cli
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
